#!/usr/bin/env perl
use common::sense;
use Mojolicious::Lite;

my $todo = {};
$todo->{1} = {
              id => 1,
              description => 'Pick up a milk',
              status => 'incomplete'};

$todo->{2} = {
              id => 2,
              description => 'Pick up a broad',
              status => 'complete'
};

get '/' => sub {
  my $c = shift;
  $c->res->headers->cache_control('max-age=1, no-cache');
  $c->stash(title => 'BackBone Plays');
  $c->render(template => 'index');
};

get '/todos/:id' => { id => 2} => sub {
  my $c = shift;
  #sleep(1);
  my $id = $c->param('id');
  $c->render(json => $todo->{$id});
};

put '/todos/:id' =>  { id => 2} => sub {
  my $c = shift;
  my $param = $c->req->json;
  say $c->dumper($param);
  $c->render(json => {status => 'ok'});
};

post '/todos/:id' =>  { id => 2} => sub {
  my $c = shift;
  my $param = $c->req->json;
  say $c->dumper($param);
  $c->render(json => {status => 'ok'});
};


app->start;
