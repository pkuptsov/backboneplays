var TodoItem = Backbone.Model.extend({
     urlRoot: '/todos',
     defaults: function() {
       return {
       date: new Date()  
       }
     }
     });
        
  var todoItem = new TodoItem({id:1});
  
  
   var TodoView = Backbone.View.extend({
     tagName: 'article',
     id: 'todo-view',
     className: 'todo',
      events: {
        "click li" : "alertTitle"
      },
    
     alertTitle: function(e) { alert(this.model.get('status')) },
     template: _.template('<li><%= description %></li>'),
     render: function() {
        this.$el.html(this.template(this.model.toJSON()));
      }
     
     });
  
  todoItem.fetch({
                 success: function(){
                 var todoView = new TodoView({ model: todoItem });
                 todoView.render();
                 $('#app').html(todoView.el);
                 }
  });